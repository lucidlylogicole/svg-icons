# SVG Icon Set

version 1.0.0

_**These icons are released to the public domain.**_  Please modify and enjoy. If you want to reference this page, that is okay.

## /icons

- A base svg icon set for commonly used icons in apps
- The icons are designed to look good at 16px and 128px
- Currently, the icon size is 256px by 256px, but that may change in the future
- icons were drawn from scratch with [Inkscape](https://inkscape.org/)
- This is intended to provide a base to modify and not a complete usable set

![](svg_preview.png)


**[View Icons in Multiple Sizes](https://lucidlylogicole.gitlab.io/svg-icons/preview.html)**

## /simple_icons

- a simpler icon set that can be used in CSS
- the icon size is 16px by 16px but they are designed to look good at larger sizes also.

![](simple_icons_preview.png)

**[View Simple Icon Set](https://lucidlylogicole.gitlab.io/svg-icons/simple_icons_preview.html)**

### Embedding SVG icon in CSS

    .menu {
        background: url('data:image/svg+xml;utf8,<svg width="16" height="16" viewbox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1"><path d="M3,4 L13,4 M3,8 L13,8 M3,12 L13,12" fill="none" stroke="currentColor" stroke-width="1.5"/></svg>');
        width: 16px;
        height: 16px;
        display:inline-block;
    }
